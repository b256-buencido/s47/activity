const textFirstName = document.querySelector ("#txt-first-name");
const spanFullName = document.querySelector ("#span-full-name");
const textLastName = document.querySelector ("#txt-last-name");
const spanFirstName = document.querySelector ("#span-first-name");

const printFullName = (event) => {
	
	spanFullName.innerHTML = (`${textFirstName.value} ${textLastName.value}`);
}

textFirstName.addEventListener ("keyup", printFullName);
textLastName.addEventListener ("keyup", printFullName);
